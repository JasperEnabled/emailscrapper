import datetime
import time
import argparse
from xml import dom
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class EmailScrapper(object):

    def __init__(self, save_path, main_url,
                 username, password, remote_url,
                 download_date=datetime.date.today()):
        self.save_path = save_path
        self.main_url = main_url
        self.username = username
        self.password = password
        self.remote_url = remote_url
        self.download_date = self._check_date(download_date)

    def _check_date(self, date):
        """
        validates if the date is in
        the given format.
        retunrs: date object.
        """
        return (date if isinstance(date, datetime.date)
                else datetime.date.fromisoformat(date))

    def get_driver(self):
        """
        loads the driver
        and returns it.
        returns: web driver
        """
        options = webdriver.FirefoxOptions()
        # options.add_argument('--headless')
        # options.add_argument("--no-sandbox")
        # options.add_argument("--disable-dev-shm-usage")
        # options.add_argument("--disable-gpu")
        options.set_preference("browser.download.dir", self.save_path)
        options.set_preference("browser.download.useDownloadDir", True)
        options.set_preference("browser.download.folderList", 2)
        options.set_preference("browser.download.viewableInternally.enabledTypes", "")
        options.set_preference("browser.helperApps.neverAsk.saveToDisk",
                               "application/pdf;text/plain;application/text;text/xml;application/xml")
        options.set_preference("pdfjs.disabled", True)

        driver = webdriver.Remote(self.remote_url,
                                  options=options)
        # driver = webdriver.Firefox(executable_path='/home/gpc/Desktop/geckodriver',
        #                           options=options)
        return driver

    def login(self, web_driver):
        """
        logs in the user in the site
        """
        username = self._get_element(web_driver=web_driver,
                                     element_id='username')
        password =self._get_element(web_driver=web_driver,
                                     element_id='passwordid')
        send = self._get_element(web_driver=web_driver,
                                     element_id='loginbutton')
        username.send_keys(self.username)
        password.send_keys(self.password)
        send.click()


    def logout_user(self, web_driver):
        """
        logs out the user
        and closes the driver
        """
        logoutbtn = self._get_element(web_driver=web_driver,
                                     element_id='leftNavLogoutLink')
        logoutbtn.click()
        time.sleep(1)
        web_driver.quit()

    def get_date_from_element(self, element, wait=1):
        """
        gets the date from the 
        element so that we can wait for the element.
        returns: str
        """
        result = element.get_attribute('date')
        if result:
            return result

        if wait < 10:
            time.sleep(wait)
            wait += 1
            return self.get_date_from_element(element, wait=wait)
        return None

    def filter_by_date(self, elements_container, web_driver):
        """
        filters the elements by
        the given date and
        returns those that match.
        returns: selenium element.
        """
        emails = (self._get_element(web_driver=web_driver, multiple=True,
                                    class_name='zfolder-msg',
                                    dom_element=elements_container,
                                    custom_duration_time=2000))
        final_emails = []
        try:
            for email in emails:
                email_date = (self._get_element(class_name='zfolder-msgDate',
                              dom_element=email, web_driver=web_driver,
                              custom_duration_time=2000))
                dated = self.get_date_from_element(email_date)
                # if not dated:
                #     import ipdb; ipdb.set_trace()

                serialized_date = dated.replace('Z','')
                formated_datetime = (datetime.datetime.
                                    fromisoformat(serialized_date))

                if formated_datetime.date() == self.download_date:
                    final_emails.append(email)
        except IndexError as ID:
            print(f'ERROR {ID}')

        return final_emails

    def scroll_to_item(self, web_driver, dom_element=None,
                       xpath=None,
                       multiple=False,
                       class_name=None,
                       element_id=None,
                       tag_name=None,
                       iterable= 100):
        """
        scrolls down and finds the item based with the given
        params.
        basically we're trying to get the field that matches
        the given related with 
        :return: item
        """
        element = None
        page_scrollable_size = int(
            web_driver.execute_script("return document.body.scrollHeight")
        )
        try:
            while iterable < page_scrollable_size:
                web_driver.execute_script(f"window.scrollTo(0, {iterable})")
                print("Scrolling", iterable)
                iterable += 100
                element = self._get_element(web_driver=web_driver,
                                            dom_element=dom_element,
                                            xpath=xpath,
                                            class_name=class_name,
                                            multiple=multiple,
                                            tag_name=tag_name,
                                            custom_duration_time=1)

        except exceptions.ElementNotInteractableException:
            iterable += 105
            print("Scrolling EX", iterable)
            web_driver.execute_script(f"window.scrollTo(0, {iterable})")
            element = self.scroll_to_item(web_driver, dom_element=dom_element,
                                          xpath=xpath,
                                          class_name=class_name,
                                          multiple=multiple,
                                          element_id=element_id,
                                          tag_name=tag_name,
                                          iterable=iterable)

        except Exception as X:
            print(X)
            raise X

        return element

    def download_files(self, element, index, web_driver):
        """
        downloads the files of the
        given element.
        : params: element:
        basically this element is the 
        item which needs to be loaded
        and once the document is downloaded
        it moves it to the next one.
        returns: None
        """
        try:
            if index == 0:
                element.click()
            else:

                next_btn = self._get_element(web_driver=web_driver, multiple=True,
                                             class_name='zmessage-scroll-button')[1]
                next_btn.click()

            # link_download_container = self._get_element(web_driver=web_driver,
            #                                             element_id='zmessage-attachments',
            #                                             custom_duration_time=5)
            time.sleep(3)
            link_download_container = web_driver.find_element_by_id('zmessage-attachments')
            # import ipdb; ipdb.set_trace()
            # files_to_download = (self._get_element(tag_name='a', web_driver=web_driver,
            #                      dom_element=link_download_container, multiple=True, wait=False))
            files_to_download = link_download_container.find_elements_by_tag_name('a')
            # import ipdb;ipdb.set_trace()
            for link in files_to_download:
                print('attachment', link.text, len(files_to_download))
                link.click()

        except exceptions.NoSuchElementException as X:
            print(f'ERROR {X}')

    def wait_until_visible(
                            self, web_driver,
                            xpath=None, class_name=None,
                            element_id=None,
                            tag_name=None,
                            duration=100, frequency=0.01
    ):
        """
        waits until the given elemenent with the xpath is visible in order to execute an specific
        acction
        :param web_driver: object driver
        :param xpath: str
        :param class_name: css class
        :param tag_name: str html tag name
        :params element_id: str html id attribute
        :param duration: time to wait for the execution if not then it passes, it's timed in seconds
        :param frequency: how quick it should pass
        :return: None
        """
        if xpath:
            WebDriverWait(web_driver, duration, frequency).until(
                EC.visibility_of_element_located((By.XPATH, xpath))
            )
        elif class_name:
            WebDriverWait(web_driver, duration, frequency).until(
                EC.visibility_of_element_located((By.CLASS_NAME, class_name))
            )
        elif element_id:
            WebDriverWait(web_driver, duration, frequency).until(
                EC.visibility_of_element_located((By.ID, element_id))
            )
        elif tag_name:
            WebDriverWait(web_driver, duration, frequency).until(
                EC.visibility_of_element_located((By.TAG_NAME, tag_name))
            )

    def _get_element(self, web_driver, dom_element=None,
                     multiple=False,
                     xpath=None,
                     class_name=None,
                     element_id=None,
                     tag_name=None,
                     custom_duration_time=100,
                     wait=True):
        """
        gets the item values
        and returns it.

        it as a special feature so that it waits for it.
        :params:
            web_driver: web driver from selenium
            dom_element: web driver element,
            in case you're trying to find in the element.
            multiple: bool. determine if you're looking for one or many.
            xpath: str: the xpath to use in order to find the element.
            class_name: str: a css class name to find the element/s by it.
            tag_name: str a tag name to find the element.
            custom_duration_time: int: time to which to wait for the element.
            element_id: str: finds an element based on the given id. only works
            for single objects.

        :returns: Web driver element/ elements depending of the multiple.
        """
        result = None
        if wait:
            self.wait_until_visible(web_driver=web_driver,
                                    xpath=xpath,
                                    class_name=class_name,
                                    element_id=element_id,
                                    tag_name=tag_name,
                                    duration=custom_duration_time)
        if dom_element and multiple:
            result = (dom_element.find_elements_by_xpath(xpath)
                      if xpath else dom_element.
                        find_elements_by_class_name(class_name)
                        if class_name else 
                        dom_element.find_elements_by_tag_name(tag_name))

        elif dom_element:
            result = (dom_element.find_element_by_xpath(xpath)
                      if xpath else dom_element.
                        find_element_by_class_name(class_name)
                        if class_name else dom_element.
                        find_element_by_id(element_id)
                        if element_id else
                        dom_element.
                        find_element_by_tag_name(tag_name))
        elif multiple:
            result = (web_driver.find_elements_by_xpath(xpath)
                      if xpath else web_driver.
                        find_elements_by_class_name(class_name)
                        if class_name else 
                        web_driver.find_elements_by_tag_name(tag_name))
        else:
            result = (web_driver.find_element_by_xpath(xpath)
                      if xpath else web_driver.
                        find_element_by_class_name(class_name)
                        if class_name else web_driver.
                        find_element_by_id(element_id)
                        if element_id else
                        web_driver.
                        find_element_by_tag_name(tag_name))
        return result

    def scrape_items(self):
        """
        scrapes all items from the
        given url.
        returns: None
        """
        driver = self.get_driver()
        driver.get(self.main_url)
        self.login(driver)
        # import ipdb; ipdb.set_trace()
        try:
            dropdown = self._get_element(web_driver=driver,
                                         element_id='leftNavFolderDropdown')
            dropdown.click()
            inbox_dropdown = (self._get_element(web_driver=driver,
                                element_id="leftNavInboxFolderLink"))
            inbox_dropdown.click()
            emails_container = self._get_element(web_driver=driver, class_name='datagrid')
            emails = self.filter_by_date(emails_container, web_driver=driver)
            self.download_files(emails[0],
                                0,
                                web_driver=driver)
            for email in emails[1:]:
                index = emails.index(email)
                self.download_files(email,index,
                                    web_driver=driver)
            self.logout_user(driver)

        except Exception as X:
            print(f'ERROR {X}')

            driver.quit()


parser = argparse.ArgumentParser()
parser.add_argument('save_path', help='path toto save the documents')
parser.add_argument('main_url', help='root location where to find the emails')
parser.add_argument('username', help='username to log in')
parser.add_argument('password', help='user password to log in')
parser.add_argument('remote_url', help='remote driver url',
                    default="http://127.0.0.1:4444")
parser.add_argument('--download_date',
                    help='date to filter the download. MUST BE YYYY-MM-DD',
                    default=datetime.date.today().isoformat())

if __name__ == '__main__':
    parsed = parser.parse_args()
    scrapper = EmailScrapper(save_path=parsed.save_path,
                            main_url=parsed.main_url,
                            username=parsed.username,
                            password=parsed.password,
                            remote_url=parsed.remote_url,
                            download_date=parsed.download_date)
    scrapper.scrape_items()
# docker run -d -p 4444:4444 --shm-size="2g" -v /home/gpc/Desktop/files_downloaded:/home/seluser/files selenium/standalone-firefox:4.1.2-20220217